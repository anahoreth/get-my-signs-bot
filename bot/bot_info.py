

from dbworker import PersonalInfo

import states.initial as initial


class BotInfo(object):

    def get_bot(self):
        return self.bot

    @property
    def personal_info(self):
        return self._personal_info

    def __init__(self, my_bot):
        self.bot = my_bot
        self._personal_info = PersonalInfo()

    def get_state(self, chat_id):
        state = self.personal_info.get_current_state(chat_id)
        return state

    def set_state(self, chat_id, state):
        self.personal_info.set_state(chat_id, state)

    def bot_send_message(self, chat_id, message, keyboard, parse_mode="Markdown"):
        if message == "":
            print("empty messssssage!!")
            return 0

        if keyboard is None:
            return self.bot.send_message(chat_id, text=message, parse_mode=parse_mode)
        else:
            return self.bot.send_message(chat_id, text=message, reply_markup=keyboard, parse_mode=parse_mode)

    def bot_edit_message(self, chat_id, message, message_id, keyboard, parse_mode="Markdown"):
        self.bot.edit_message_text(chat_id=chat_id, text=message, message_id=message_id,
                                   reply_markup=keyboard, parse_mode=parse_mode)

    def handle_command(self, message):

        chat_id = message.chat.id

        command_name = message.text

        state = self.get_state(chat_id)

        if command_name == "/start":
            self.personal_info.init_chat(chat_id)
            next_state = initial.GetNameState(self, chat_id)
            self.set_state(chat_id, next_state)
        else:
            state.handle_command(message)
            next_state = state.get_next_state()
            self.set_state(chat_id, next_state)

    def handle_message(self, message):
        chat_id = message.chat.id
        state = self.get_state(chat_id)
        if state is None:
            print("УККЩЩЩЩЩЩ")
            return
        state.handle_message(message)
        next_state = state.get_next_state()
        if next_state is not None:
            self.set_state(chat_id, next_state)

    def handle_query(self, call):

        chat_id = call.message.chat.id
        state = self.get_state(chat_id)
        if state is None:
            self.personal_info.init_chat(chat_id)
        state.handle_query(call)
        next_state = state.get_next_state()
        if next_state is not None:
            self.set_state(chat_id, next_state)

