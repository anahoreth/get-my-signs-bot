from enum import Enum


TOKEN = "1384091461:AAEio8nKqsXXyXZXVAr-lxPuRA8zmjTM6qk"


# признак того, что бот запускается локально
IS_LOCAL = False

IS_REAL_BOT = True

IS_TEST = False

if IS_TEST:
    IS_LOCAL = True

    IS_DEMO = False

    IS_REAL_BOT = False


class PersonalInfoKeys(Enum):
    KeyState = "state"
    KeyCommissionName = "com_name"
    KeyPersonName = "person_name"
    KeyAddress = "address"
    KeyPhoneNumber = "phone_number"
    KeyEmail = "email"
    KeyPassportNumber = 'passport_number'
    KeyPassportDate = 'passport_date'
    KeyCandidateName = 'candidate_name'
    KeyDate = "date"
    KeyIsMan = 'is_man'
    KeyPlaceCity = "place_city"
    KeyPlace = "place"
