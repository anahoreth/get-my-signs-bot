

from config import TOKEN  # подключаем конфиг, чтобы взять с него токен бота
from config import IS_LOCAL, IS_REAL_BOT

import bot_info

if IS_REAL_BOT:
    import telebot


bot = telebot.TeleBot(TOKEN)
my_bot_info = bot_info.BotInfo(bot)


@bot.message_handler(commands=['start'])
def command_handler(message):
    my_bot_info.handle_command(message)


@bot.message_handler(func=lambda message: True)
def message_handler(message):
    my_bot_info.handle_message(message)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    my_bot_info.handle_query(call)


if __name__ == '__main__':
    if IS_LOCAL:
        my_bot_info.bot.remove_webhook()
    my_bot_info.bot.polling(none_stop=True)

