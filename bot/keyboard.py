from config import IS_REAL_BOT

if IS_REAL_BOT:
    from telebot import types
    from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup

from enum import Enum


class YesNoButtons(Enum):
    Yes = 'Да'
    No = 'Нет'


YesNoButtonsList = [[YesNoButtons.Yes.value, YesNoButtons.No.value]]


class SexButtons(Enum):
    Man = 'Мужском'
    Woman = 'Женском'


SexButtonsList = [[SexButtons.Man.value, SexButtons.Woman.value]]


# TODO на такой клавиатуре если не "Да", то автоматически "нет". МОжно добавить другие проверки
def is_yes(string):
    return string == YesNoButtons.Yes.value


def get_reply_keyboard_from_list(button_text_list):
    if not IS_REAL_BOT:
        return None
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
    for button_text in button_text_list:
        button = types.KeyboardButton(text=button_text)
        keyboard.add(button)
    return keyboard


def get_answers_keyboard(answers_list):
    if not IS_REAL_BOT:
        return None

    keyboard = InlineKeyboardMarkup()

    for answers_row in answers_list:

        if isinstance(answers_row, str):
            keyboard.row(InlineKeyboardButton(answers_row, callback_data=answers_row))
            continue
        row = []
        for answer in answers_row:
            button = InlineKeyboardButton(answer, callback_data=answer)
            row.append(button)
        keyboard.row(*row)
    return keyboard


def get_yes_no_keyboard():
    return get_answers_keyboard(YesNoButtonsList)


def get_sex_keyboard():
    return get_answers_keyboard(SexButtonsList)


class Cities(Enum):
    CITY_MINSK = 'г. Минск'
    CITY_BREST_REGION = 'Брестская область'
    CITY_VITEBSK_REGION = 'Витебская область'
    CITY_GOMEL_REGION = 'Гомельская область'
    CITY_GRODNO_REGION = 'Гродненская область'
    CITY_MINSK_REGION = 'Минская область'
    CITY_MOGILEV_REGION = 'Могилевская область'


def get_cities_list():
    return [city.value for city in Cities]


candidates_list = \
    ["Виктор Бабарико",
     "Андрей Дмитриев",
     "Анна Канопацкая",
     "Светлана Тихановская",
     "Сергей Черечень",
     "Валерий Цепкало",
     "Александр Лукашенко (а вдруг?)"]

candidates_in_complain_dict = {
    "Виктор Бабарико": "Виктора Бабарико",
    "Андрей Дмитриев": "Андрея Дмитриева",
    "Анна Канопацкая": "Анны Канопацкой",
    "Светлана Тихановская": "Светланы Тихановской",
    "Сергей Черечень": "Сергея Череченя",
    "Валерий Цепкало": "Валерия Цепкало",
    "Александр Лукашенко (а вдруг?)": "Александра Лукашенко"
}


def get_city_keyboard():
    return get_reply_keyboard_from_list(get_cities_list())


def get_candidate_keyboard():
    return get_reply_keyboard_from_list(candidates_list)


def get_places_list(city):
    return PLACE_DICT.get(city)


def get_place_keyboard(city):
    return get_reply_keyboard_from_list(get_places_list(city))


PLACE_DICT = {Cities.CITY_BREST_REGION.value: [
    "Брестская областная избирательная комиссия",
    "Окружная избирательная комиссия Брестского-Западного избирательного округа № 1",
], Cities.CITY_VITEBSK_REGION.value: [
    "Витебская областная избирательная комиссия"
], Cities.CITY_GOMEL_REGION.value:
    [
        "Гомельская областная избирательная комиссия"
    ],
    Cities.CITY_GRODNO_REGION.value:
        [
            "Гродненская областная избирательная комиссия"
        ],
    Cities.CITY_MINSK_REGION.value:
        [
            "Минская областная избирательная комиссия"
        ],
    Cities.CITY_MINSK.value:
        [
            "Минская городская избирательная комиссия"
        ],
    Cities.CITY_MOGILEV_REGION.value:
        [
            "Могилевская областная избирательная комиссия"
        ]
}
