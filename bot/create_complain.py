import config

if config.IS_REAL_BOT:
    from docx import Document
    from docx.shared import Pt, Cm
    from docx.enum.text import WD_ALIGN_PARAGRAPH, WD_LINE_SPACING
from config import PersonalInfoKeys

from messages import get_string_date
from keyboard import candidates_in_complain_dict

complaint_name = "ОБРАЩЕНИЕ"

complain_second_paragraph_text = "Дата и подпись вносились мной в подписной лист собственноручно" \
                                 " в присутствии уполномоченного члена инициативной группы, данные " \
                                 "которого были указаны в подписном листе."

complain_third_paragraph_text = "В соответствии с частью первой статьи 13 Избирательного кодекса Республики Беларусь " \
                                "подготовка и проведение выборов Президента Республики Беларусь осуществляются" \
                                " открыто и гласно. Согласно части второй вышеупомянутой статьи, " \
                                "соответствующие комиссии, местные представительные, " \
                                "исполнительные и распорядительные органы информируют граждан о своей работе " \
                                "по подготовке и проведению выборов. В соответствии со ст. 3 Закона Республики " \
                                'Беларусь от 18.07.2011 № 300-З "Об обращениях граждан и юридических лиц" ' \
                                'граждане имеют право на обращение в организации путем подачи письменных, электронных' \
                                ' или устных обращений.'


class MyDocument:

    def __init__(self):
        self.document = Document()
        self.document.styles['Normal'].font.name = 'Times New Roman'
        self.document.styles['Normal'].font.size = Pt(14)
        paragraph_format = self.document.styles['Normal'].paragraph_format
        paragraph_format.space_before = Pt(0)
        paragraph_format.space_after = Pt(0)
        paragraph_format.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph_format.first_line_indent = Cm(1.25)

        sections = self.document.sections
        for section in sections:
            section.top_margin = Cm(2)
            section.bottom_margin = Cm(2)
            section.left_margin = Cm(2)
            section.right_margin = Cm(1)

    def add_person_info(self, personal_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.RIGHT

        place = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPlace)
        if place is not None:
            p.add_run(place + "\n")
        else:
            p.add_run("[Наименование комиссии]\n")

        name = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPersonName)
        address = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyAddress)
        phone_number = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPhoneNumber)
        email = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyEmail)

        p.add_run(name + "\n")

        if address is not None:
            p.add_run("Адрес: " + address + "\n")
        if phone_number is not None:
            p.add_run("Телефон: " + phone_number + "\n")
        if email is not None:
            p.add_run("Электронная почта: " + email + "\n")

    def add_title(self):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p.add_run(complaint_name + '\n').bold = True

    def add_first_paragraph(self, personal_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        name = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPersonName)
        if name is None:
            name = " "
        address = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyAddress)
        if address is None:
            address = " "
        passport_number = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPassportNumber)
        if passport_number is None:
            passport_number = " "
        passport_date = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPassportDate)
        if passport_date is None:
            passport_date = " "
        candidate_name = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyCandidateName)
        if candidate_name is None:
            candidate_name = " "
        elif candidates_in_complain_dict.get(candidate_name) is not None:
            candidate_name = candidates_in_complain_dict.get(candidate_name)

        is_man = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyIsMan)
        citizen = "гражданин"
        regist = "зарегистрован"
        take = "поставил"
        if not is_man:
            citizen = "гражданка"
            regist += "а"
            take += "а"

        message = "Я, " + name + ", " + citizen + " Республики Беларусь, " + regist + " по " \
                                                                                      "адресу: " + address + \
                  ", паспорт № " + passport_number + ", выданный " + passport_date + \
                  ", " + take + " свою подпись за выдвижение в кандидаты в Президенты Республики Беларусь " + \
                  candidate_name + "."

        p.add_run(message)

    def add_second_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        p.add_run(complain_second_paragraph_text)

    def add_third_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        p.add_run(complain_third_paragraph_text)

    def add_fourth_paragraph(self, person_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

        complain_fourth_paragraph_text = "На основании изложенного,"
        p.add_run(complain_fourth_paragraph_text)
        p = self.document.add_paragraph('')

        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p.add_run("ПРОШУ" + '\n').bold = True

        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        message = \
            "1. Сообщить информацию о проведении проверки подлинности моей подписи в соответствии со статьей 61 " \
            "Избирательного кодекса Республики Беларусь. В случае, если моя подпись подлежала проверке, прошу сообщить " \
            "о ее результатах. В случае, если моя подпись была признана недостоверной, " \
            "прошу сообщить об основаниях признания подписи недостоверной."
        p.add_run(message)

        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
        message = "2. Ответ направить по адресу и электронной почте, указанным в настоящем заявлении."

        p.add_run(message)

    def add_sign(self, personal_info, chat_id):
        p = self.document.add_paragraph('')
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        name = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPersonName)
        if name is None:
            name = " "

        date = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyDate)
        if date is None:
            date = " "

        p.add_run("\n" + name + "\n\n\n")
        p.add_run(get_string_date(date) + "\n")

    def get_document(self):
        return self.document


def make_complain(personal_info, chat_id):
    my_document = MyDocument()

    my_document.add_person_info(personal_info, chat_id)
    my_document.add_title()

    my_document.add_first_paragraph(personal_info, chat_id)
    my_document.add_second_paragraph(personal_info, chat_id)
    my_document.add_third_paragraph(personal_info, chat_id)
    my_document.add_fourth_paragraph(personal_info, chat_id)

    my_document.add_sign(personal_info, chat_id)

    return my_document.get_document()


'''
import dbworker
from config import PersonalInfoKeys

id = 0
test_com_name = "commission_name"
test_name = "Иванов Иван Иванович"
test_address = "address"
test_phone_number = "+375 (29) 111 - 11 - 11"
test_email = "email"
test_PassportNumber = 'passport_number'
test_PassportDate = '01 января 2020'
test_CandidateName = 'Иванов Иван Иванович'

test_year = 2020
test_month = 10
test_day = 10
test_date = (test_year, test_month, test_day)


pi = dbworker.PersonalInfo()
pi.set_parameter(id, PersonalInfoKeys.KeyCommissionName, test_com_name)
pi.set_parameter(id, PersonalInfoKeys.KeyPersonName, test_name)
pi.set_parameter(id, PersonalInfoKeys.KeyAddress, test_address)
pi.set_parameter(id, PersonalInfoKeys.KeyPhoneNumber, test_phone_number)
pi.set_parameter(id, PersonalInfoKeys.KeyEmail, test_email)
pi.set_parameter(id, PersonalInfoKeys.KeyPassportNumber, test_PassportNumber)
pi.set_parameter(id, PersonalInfoKeys.KeyPassportDate, test_PassportDate)
pi.set_parameter(id, PersonalInfoKeys.KeyCandidateName, test_CandidateName)

complaint = make_complain(pi, id)

DOCUMENT_FILENAME = "_complain.docx"
complaint_filename = str(id) + DOCUMENT_FILENAME
complaint.save(complaint_filename)
'''
