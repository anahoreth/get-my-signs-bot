

from messages import MESSAGE_GET_PLACE

from config import PersonalInfoKeys

from keyboard import get_places_list, get_place_keyboard

import states.abstract as abstract
import states.form_complain as form_complain


class StatePlaceGetPlace(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        city = personal_info.get_parameter(chat_id, PersonalInfoKeys.KeyPlaceCity)
        keyboard = get_place_keyboard(city)
        message = MESSAGE_GET_PLACE
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    # TODO обработка того, что место неправильно введено
    def handle_message(self, message) -> None:
        place = message.text
        personal_info = self.bot_info.personal_info
        if self.is_correct_place(place):
            personal_info.set_parameter(self.chat_id, PersonalInfoKeys.KeyPlace, place)
        next_state = form_complain.StateFormComplain(self.bot_info, self.chat_id)
        self.set_next_state(next_state)

    # дополнительные
    def is_correct_place(self, place):
        personal_info = self.bot_info.personal_info
        city = personal_info.get_parameter(self.chat_id, PersonalInfoKeys.KeyPlaceCity)
        return place in get_places_list(city)
