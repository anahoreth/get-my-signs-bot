
from messages import *

from config import PersonalInfoKeys, IS_LOCAL
from keyboard import get_sex_keyboard, SexButtons
from keyboard import get_candidate_keyboard


import states.abstract as abstract
import states.get_city as get_city


# получение имени пользователя
class GetNameState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_PERSONAL_INFO, None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        name = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyPersonName, name)
        next_state = GetSex(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение пола
class GetSex(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_SEX, get_sex_keyboard()
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message):
        pass

    def handle_query(self, call):
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        sex = call.data
        is_man = sex == SexButtons.Man.value
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyIsMan, is_man)
        next_state = GetAddressState(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение адреса пользователя
class GetAddressState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_ADDRESS, None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        name = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyAddress, name)
        next_state = GetPhoneNumber(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение номера телефона
class GetPhoneNumber(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_PHONE_NUMBER, None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        name = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyPhoneNumber, name)
        next_state = GetEmailState(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение эл. почты
class GetEmailState(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_EMAIL, None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        email = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyEmail, email)
        next_state = GetPassportNumber(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение номера паспорта
class GetPassportNumber(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_PASSPORT_NUMBER, None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        email = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyPassportNumber, email)
        next_state = GetPassportDate(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение дату получения паспорта
class GetPassportDate(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message, keyboard = MESSAGE_GET_PASSPORT_DATE, None
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        email = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyPassportDate, email)
        next_state = GetCandidateName(self._bot_info, chat_id)
        self.set_next_state(next_state)


# получение имени кандидата
class GetCandidateName(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        chat_id = self._chat_id
        message = MESSAGE_GET_CANDIDATE_NAME
        keyboard = get_candidate_keyboard()
        self._bot_info.bot_send_message(chat_id, message, keyboard)

    def handle_message(self, message) -> None:
        cand_name = message.text
        chat_id = self._chat_id
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyCandidateName, cand_name)
        next_state = get_city.StatePlaceGetCity(self._bot_info, chat_id)
        self.set_next_state(next_state)
