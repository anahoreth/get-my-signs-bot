from abc import abstractmethod


from keyboard import YesNoButtonsList, get_answers_keyboard


class BotState(object):

    @property
    def bot_info(self):
        return self._bot_info

    @property
    def chat_id(self):
        return self._chat_id

    def __init__(self, bot_info, chat_id):
        self._bot_info = bot_info
        self._chat_id = chat_id
        self._next_state = None

    def set_next_state(self, state):
        if state is None:
            raise Exception("None state!")
        self._next_state = state

    def get_next_state(self):
        return self._next_state

    @abstractmethod
    def sent_start_message(self):
        return None

    def handle_message(self, message) -> None:
        pass

    def handle_query(self, call) -> None:
        pass


class QuestionWithAnswersState(BotState):

    def __init__(self, bot_info, chat_id, start_message, answers_list):
        super().__init__(bot_info, chat_id)
        self.message = start_message
        self.answers_list = answers_list
        self.start_message_id = self.sent_start_message()

    def sent_start_message(self):
        chat_id = self._chat_id
        keyboard = get_answers_keyboard(self.answers_list)
        message = self.message
        return self._bot_info.bot_send_message(chat_id, message, keyboard).message_id

    def handle_message(self, message) -> None:
        pass

    @abstractmethod
    def answer_action(self, answer_index):
        pass

    def handle_query(self, call) -> None:
        answer = call.data
        self.answer_action(answer)
        self.remove_keyboard()

    def remove_keyboard(self):
        message = self.message
        keyboard = None
        self._bot_info.bot_edit_message(self.chat_id, message=message, keyboard=keyboard,
                                        message_id=self.start_message_id)


class YesNoQuestionState(QuestionWithAnswersState):

    def __init__(self, bot_info, chat_id, start_message):
        super().__init__(bot_info, chat_id, start_message, YesNoButtonsList)

    def answer_action(self, answer):
        pass