from config import IS_LOCAL
import create_complain
from messages import DOCUMENT_FILENAME, DOCUMENT_SUCCESS, DOCUMENT_ERROR
import datetime
from config import PersonalInfoKeys

import os
if IS_LOCAL:
    import abstract
else:
    import states.abstract as abstract


class StateFormComplain(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self) -> None:
        self.make_complain()

    def handle_message(self, message) -> None:
        pass

    def make_complain(self):
        chat_id = self.chat_id
        personal_info = self.bot_info.personal_info
        date_today = datetime.datetime.now()
        time = (date_today.year, date_today.month, date_today.day)

        personal_info.set_parameter(chat_id, PersonalInfoKeys.KeyDate, time)

        complaint = create_complain.make_complain(personal_info, chat_id)
        complaint_filename = str(chat_id) + DOCUMENT_FILENAME
        complaint.save(complaint_filename)

        self.delete_data(chat_id)

        bot = self.bot_info.bot

        sent_result = self.send_document(complaint_filename)
        if sent_result:
            bot.send_message(chat_id, DOCUMENT_SUCCESS)
        else:
            bot.send_message(chat_id, DOCUMENT_ERROR)

    def delete_data(self, chat_id):
        personal_info = self.bot_info.personal_info
        for a in PersonalInfoKeys:
            personal_info.set_parameter(chat_id, a, 0)

    def send_document(self, file_name):
        chat_id = self._chat_id
        bot = self.bot_info.bot
        if os.path.exists(file_name):
            doc = open(file_name, 'rb')
            bot.send_document(chat_id, doc)
            return True
        else:
            return False
