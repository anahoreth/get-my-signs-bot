

from messages import MESSAGE_GET_REGION


from config import PersonalInfoKeys

from keyboard import get_city_keyboard

import states.abstract as abstract
import states.get_place as get_place


class StatePlaceGetCity(abstract.BotState):
    def __init__(self, bot_info, chat_id):
        super().__init__(bot_info, chat_id)
        self.sent_start_message()

    def sent_start_message(self):
        keyboard = get_city_keyboard()
        message = MESSAGE_GET_REGION
        self._bot_info.bot_send_message(self.chat_id, message, keyboard)

    # TODO обработка того, что город неправильно введен
    def handle_message(self, message) -> None:
        city = message.text
        personal_info = self.bot_info.personal_info
        personal_info.set_parameter(self.chat_id, PersonalInfoKeys.KeyPlaceCity, city)
        next_state = get_place.StatePlaceGetPlace(self.bot_info, self.chat_id)
        self.set_next_state(next_state)
