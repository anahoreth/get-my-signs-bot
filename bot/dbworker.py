from config import PersonalInfoKeys


class PersonalInfo(object):
    def __init__(self):
        self.my_db = {}

    def init_chat(self, chat_id):
        self.my_db[chat_id] = {}

    def set_state(self, chat_id, state):
        self.my_db[chat_id][PersonalInfoKeys.KeyState] = state

    def get_current_state(self, chat_id):
        not_inited = self.my_db.get(chat_id) is None
        if not_inited:
            self.init_chat(chat_id)
            return None
        not_state = (self.my_db[chat_id]).get(PersonalInfoKeys.KeyState) is None
        if not_state:
            self.init_chat(chat_id)
            return None
        state = self.my_db[chat_id][PersonalInfoKeys.KeyState]
        return state

    def set_parameter(self, chat_id, parameter_name, value):
        not_inited = self.my_db.get(chat_id) is None
        if not_inited:
            self.init_chat(chat_id)
            return None
        self.my_db[chat_id][parameter_name] = value

    def get_parameter(self, chat_id, parameter_name):
        try:
            return self.my_db[chat_id][parameter_name]
        except:
            print("ERROR:", parameter_name)
            return None
