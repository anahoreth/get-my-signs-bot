
import os
import flask
from telebot import types
# from config import *
# import bot

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, 'bot')

TOKEN = "1384091461:AAEio8nKqsXXyXZXVAr-lxPuRA8zmjTM6qk"
APP_NAME = 'myelectionapp'


from bot.bot_handler import my_bot_info

server = flask.Flask(__name__)


@server.route('/' + TOKEN, methods=['POST'])
def get_message():
    my_bot_info.get_bot().process_new_updates([types.Update.de_json(
        flask.request.stream.read().decode("utf-8"))])
    return "!", 200


@server.route('/', methods=["GET"])
def index():
    my_bot_info.get_bot().remove_webhook()
    my_bot_info.get_bot().set_webhook(url="https://{}.herokuapp.com/{}".format(APP_NAME, TOKEN))
    return "Hello from Heroku!", 200


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)))
