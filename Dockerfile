# Dockerfile
FROM python
RUN pip install pyTelegramBotAPI python-docx flask gunicorn xlrd openpyxl pandas
ADD bot /bot
ADD run_server.py /
CMD ["gunicorn", "run_server:server" ]
